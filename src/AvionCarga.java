public class AvionCarga extends Avion{
   //////////Constructor//////////////
    public AvionCarga(String color, double tamanho){
        super(color,tamanho);///////////Hereda los atributos de la superclase
    }

    /////////Métodos (Acciones)//////////
    public void cargar(){
        System.out.println("Cargando bodega...");
    }
    public void descargar(){
        System.out.println("Descargando bodega...");
    }
}