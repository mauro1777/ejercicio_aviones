public class AvionPasajeros extends Avion {
    /////////Atributo nuevo//////////////
    private int pasajeros;
    
    //////////Constructor//////////////
    public AvionPasajeros(String color, double tamanho, int pasajeros){
        super(color,tamanho);///////////Hereda los atributos de la superclase
        this.pasajeros=pasajeros;
    }

    /////////Métodos (Acciones)//////////
    public void servir(){
        for (int i=1;i<=pasajeros;i++){
        System.out.println("Atendiendo al pasajero- "+i);
        }
    }
}
