public class App {
    public static void main(String[] args) throws Exception {
        //Construir un avion de carga
        /*
        AvionCarga objAvionCarga = new AvionCarga("Gris", 150.5);
        objAvionCarga.cargar();
        System.out.println(objAvionCarga.despegar());
        objAvionCarga.aterrizar();
        objAvionCarga.descargar();    
        */
        /*   
        AvionPasajeros objAvionPasajeros = new AvionPasajeros("Azul", 200.2, 140);
        objAvionPasajeros.servir();
        */

        AvionMilitar objAvionMilitar = new AvionMilitar("Azul", 12.5);
        objAvionMilitar.setMisiles(6);
        objAvionMilitar.despegar();
        objAvionMilitar.detectar_amenaza(false);
        for(int i = 0; i < 8; i++){
            objAvionMilitar.detectar_amenaza(true);
        }
        
    }
}
